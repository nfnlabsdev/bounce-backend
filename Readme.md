
## Getting Started

### Prerequisites

- Python 3.9 or higher
- PostgreSQL

### Creating virtual environment 📦

- Using [virtualenv](https://virtualenv.pypa.io/en/latest/) 📦
- Create a `virtual environment` for this project 📦

```shell
# creating virtual environment
$ virtualenv venv

# activating the virtual environment
$ source venv/bin/activate

# installing dependencies
$ pip install -r requirements.txt
```

### Database Migration


- Migrate and upgrade database into your database management (for this case postgreeSQL)

```sh
flask db init

flask db migrate -m "create new table"

flask db upgrade
```

### Running app

- If you feel that everything can be run, then run the Flask API

```sh
flask run
```
