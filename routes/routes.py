from app import app
from flask import request, session, Response, jsonify, make_response, send_file, render_template, redirect, url_for

from app import db
from flask_socketio import SocketIO
from flask_mqtt import Mqtt

import sys
import os
import requests
app.secret_key = os.urandom(24)

from app.Controller import vehicle_controller, user_controller, mqtt_controller, geofence_controller
from app.Models.user import User
from app.Models.geofence import Geofence
from app.Models.vehicle import Vehicle
from app.Utility import fcm, detailsByVcu, detailsByuid


# @app.before_request
def before_request_func():
    token = request.headers.get("infinityToken")
    # print({"logged_token": request.headers
    # token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjI1NjA4NCwiaXNzIjoiaHR0cDovL3d3dy5tZXRyb2Jpa2VzLmluL2FwaS9zZGtfdmVyaWZ5X3VzZXIiLCJpYXQiOjE1Mzc5NTU1NTksImV4cCI6MjEzNzk1NTU1OSwibmJmIjoxNTM3OTU1NTU5LCJqdGkiOiJHZUNkcTVsVFBJUFcxcmM2In0.Xk7x9H0dMtMIz64ZBuab3QqY5p4erU6kG3WXOKJI7WQ"
    if token is not None:
        r = requests.post('http://stagingbcore.bounce.bike' + '/authenticate', headers={'AppToken': 'ykD0aGLjXRecH521aqJk', 'token': token})
        user_response = r.json()['data']
        print({"logged_user": user_response})
        session["user_object"] = user_response

        try:
            print("session_uid", session["user_object"]["uid"])
            session["vehicle_object"] = detailsByuid.userDetails(session["user_object"]["uid"])[0]
            print("session_vehicle", session["vehicle_object"])
            return session
        except Exception as e:
            print ('Authorization failed')
            return None

    else:
        print ('Authorization failed')
        return None

@app.route('/file/<path:path>')
def get_image(path):
    return send_file('./static/' + path, mimetype='image/gif')

@app.route('/')
def home():
    if before_request_func() is None: return Response('Authorization failed', status=401)
    return "Flask " + sys.version

@app.route('/admin/login')
def admin_login():
  if not session.get('logged_in'):
    return render_template('login.html')
  else:
    return render_template('index.html')

@app.route('/login_post')
def login_post():
    # return render_template('index.html')
    session['logged_in'] = True
    base_url = request.base_url.split('login_post')[0] + 'admin/'
    return redirect(base_url)
  
@app.route('/sample', methods=['POST'])
def sample():
    return user_controller.sample()

@app.route('/sample2', methods=['POST'])
def sample2():
    return user_controller.sample2()

@app.route('/scheduled/anti_theft')
def scheduled_anti_theft():
    return user_controller.scheduled_anti_theft()

@app.route('/app/ring', methods=['GET'])
def ring():
    if before_request_func() is None: return Response('Authorization failed', status=401)
    return mqtt_controller.ring_scooter()

@app.route('/notification_token', methods=['POST'])
def notification_token():
    if before_request_func() is None: return Response('Authorization failed', status=401)
    return vehicle_controller.notification_token()

@app.route('/api/home', methods=['GET'])
def app_home():
    if before_request_func() is None: return Response('Authorization failed', status=401)
    return mqtt_controller.home()

@app.route('/profile', methods=['POST','GET'])
def profile_update():
    if before_request_func() is None: return Response('Authorization failed', status=401)
    if request.method == 'POST':
        return user_controller.profile_update()
    else:
        return user_controller.get_profile()

@app.route('/vehicle/location', methods=['GET'])
def vehicle_location():
    if before_request_func() is None: return Response('Authorization failed', status=401)
    if request.method == 'GET':
        return mqtt_controller.vehicle_location()

@app.route('/vehicle_manual')
def vehicle_manual():
    return vehicle_controller.vehicle_manual()

@app.route('/dms/vehicles', methods=['POST','PUT','DELETE'])
def dms():
    if before_request_func() is None: return Response('Authorization failed', status=401)
    if request.method == 'POST':
        return vehicle_controller.dms_data_post()
    elif request.method == 'PUT':
        return vehicle_controller.dms_data_update()
    else:
        return vehicle_controller.dms_data_delete()

@app.route('/dms/vcu_update', methods=['PUT'])
def dms_vcu_update():
    if before_request_func() is None: return Response('Authorization failed', status=401)
    return vehicle_controller.dms_vcu_update()

@app.route('/vcu/errors',)
def vcu_errors():
    if before_request_func() is None: return Response('Authorization failed', status=401)
    return vehicle_controller.vcu_errors()

@app.route('/mqtt_test', methods=['GET', 'POST'])
def mqtt():
    if before_request_func() is None: return Response('Authorization failed', status=401)
    if request.method == 'GET':
        return mqtt_controller.flask_mqtt_message()
    elif request.method == 'POST':
        return mqtt_controller.flask_mqtt_publish()

@app.route('/mqtt/publish', methods=['POST'])
def mqtt_publish():
    if before_request_func() is None: return Response('Authorization failed', status=401)
    # if request.method == 'GET':
    #     return mqtt_controller.flask_mqtt_message()
    # elif request.method == 'POST':
    return mqtt_controller.mqtt_publish_values()

@app.route('/mqtt/details', methods=['GET', 'POST'])
def mqttDetails():
    if before_request_func() is None: return Response('Authorization failed', status=401)
    if request.method == 'GET':
        return mqtt_controller.latestVehicleStatus()
    elif request.method == 'POST':
        return mqtt_controller.latestVehicleStatus()

@app.route('/geofence/user_response', methods=['post'])
def geofence_user_response():
    if before_request_func() is None: return Response('Authorization failed', status=401)
    return geofence_controller.geofence_user_response()

@app.route('/anti_theft/user_response', methods=['post'])
def anti_theft_user_response():
    if before_request_func() is None: return Response('Authorization failed', status=401)
    return geofence_controller.anti_theft_user_response()

@app.route('/api/geofence', methods=['GET','POST','PUT','DELETE'])
def geofenceMainApis():
    if before_request_func() is None: return Response('Authorization failed', status=401)
    if request.method == 'POST':
        return geofence_controller.save()
    elif request.method == 'GET':
        return geofence_controller.list()
    elif request.method == 'PUT':
        return geofence_controller.geofence_update()

@app.route('/geofence/state', methods=['PUT', 'GET', 'POST'])
def geofenceOtherApis():
    if before_request_func() is None: return Response('Authorization failed', status=401)
    if request.method == 'PUT': return geofence_controller.switch_state()
    elif request.method == 'POST': return geofence_controller.switch_notifiy_state()
    elif request.method == 'GET': return geofence_controller.get_notifiy_state()

@app.route('/geofence/breaches', methods=['POST', 'GET'])
def geofenceBreachApis():
    if before_request_func() is None: return Response('Authorization failed', status=401)
    if request.method == 'POST':
        return geofence_controller.find_geofence_breach()
    elif request.method == 'GET':
        return geofence_controller.get_breached_geofences()

@app.route('/api/vehicles', methods=['GET', 'POST'])
def vehicles():
    if before_request_func() is None: return Response('Authorization failed', status=401)
    if request.method == 'GET':
        return vehicle_controller.lists()
    else:
        return vehicle_controller.save()

@app.route('/api/users', methods=['GET', 'POST'])
def users():
    if request.method == 'GET':
        return user_controller.lists()
    else:
        return user_controller.save()
