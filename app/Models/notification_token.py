from app import db
from datetime import datetime
from app.Models.vehicle import Vehicle

class NotificationToken(db.Model):
    __tablename__ = 'notification_tokens'
    id = db.Column(db.BigInteger, primary_key=True, autoincrement=True)
    vcu_id = db.Column(db.String(100), db.ForeignKey('vehicles.vcu_id'))
    token_str = db.Column(db.String(1000), nullable=True)
    whatever = db.Column(db.String(100), nullable=True)
    is_active = db.Column(db.Boolean, nullable=False, default=True)
    created_at = db.Column(db.DateTime, default=datetime.utcnow)
    updated_at = db.Column(db.DateTime, default=datetime.utcnow)

    def __repr__(self):
        return '<NotificationToken {}>'.format(self.name)
