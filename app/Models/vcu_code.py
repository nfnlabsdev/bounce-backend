from app import db
from datetime import datetime

class VcuCode(db.Model):
    __tablename__ = 'vcu_codes'
    id = db.Column(db.BigInteger, primary_key=True, autoincrement=True)
    code = db.Column(db.String(100), index=True, nullable=True)
    message = db.Column(db.String(255), nullable=True)
    message_type = db.Column(db.String(15), nullable=True)
    is_active = db.Column(db.Boolean, nullable=False, default=True)
    created_at = db.Column(db.DateTime, default=datetime.utcnow)
    updated_at = db.Column(db.DateTime, default=datetime.utcnow)

    def __repr__(self):
        return '<VcuCode {}>'.format(self.name)
