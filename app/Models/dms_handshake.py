from app import db
from datetime import datetime
from app.Models.vehicle import Vehicle
from sqlalchemy.dialects.mysql import LONGTEXT

class DmsHandshake(db.Model):
    __tablename__ = 'dms_handshakes'
    id = db.Column(db.BigInteger, primary_key=True, autoincrement=True)
    vcu_id = db.Column(db.String(100), db.ForeignKey('vehicles.vcu_id'))
    purpose = db.Column(db.String(300))
    initiated_by = db.Column(db.Boolean, nullable=False, default=True)
    dms_content = db.Column(db.String(64000))
    is_active = db.Column(db.Boolean, nullable=False, default=True)
    created_at = db.Column(db.DateTime, default=datetime.utcnow)
    updated_at = db.Column(db.DateTime, default=datetime.utcnow)

    def __repr__(self):
        return '<DmsHandshake {}>'.format(self.name)
