from app import db
from datetime import datetime
from app.Models.vehicle import Vehicle

class VcuDetail(db.Model):
    __tablename__ = 'vcu_details'
    id = db.Column(db.BigInteger, primary_key=True, autoincrement=True)
    vcu_id = db.Column(db.String(100), db.ForeignKey('vehicles.vcu_id'))
    imei_code = db.Column(db.String(100), nullable=True)
    vcu_type = db.Column(db.String(100), nullable=True)
    is_active = db.Column(db.Boolean, nullable=False, default=True)
    created_at = db.Column(db.DateTime, default=datetime.utcnow)
    updated_at = db.Column(db.DateTime, default=datetime.utcnow)

    def __repr__(self):
        return '<VcuDetail {}>'.format(self.name)
