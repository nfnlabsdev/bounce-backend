from app import db
from datetime import datetime
from app.Models.vehicle import Vehicle

class GeofenceBreach(db.Model):
    __tablename__ = 'geofence_breaches'
    id = db.Column(db.BigInteger, primary_key=True, autoincrement=True)
    vcu_id = db.Column(db.String(100), db.ForeignKey('vehicles.vcu_id'))
    geofence_id = db.Column(db.Integer, db.ForeignKey('geofences.id'))
    location_lat = db.Column(db.FLOAT(precision=10), nullable=False)
    location_long = db.Column(db.FLOAT(precision=10), nullable=False)
    user_response = db.Column(db.String(100), nullable=True)
    mqtt_content = db.Column(db.String(64000), nullable=True)
    is_active = db.Column(db.Boolean, nullable=False, default=True)
    created_at = db.Column(db.DateTime, default=datetime.utcnow)
    updated_at = db.Column(db.DateTime, default=datetime.utcnow)

    def __repr__(self):
        return '<GeofenceBreach {}>'.format(self.name)
