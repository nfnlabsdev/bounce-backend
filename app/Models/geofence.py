from app import db
from datetime import datetime
from app.Models.vehicle import Vehicle

class Geofence(db.Model):
    __tablename__ = 'geofences'
    id = db.Column(db.BigInteger, primary_key=True, autoincrement=True)
    vcu_id = db.Column(db.String(100), db.ForeignKey('vehicles.vcu_id'))
    name = db.Column(db.String(100), nullable=True)
    address = db.Column(db.String(500), nullable=True)
    landmark = db.Column(db.String(100), nullable=True)
    location_lat = db.Column(db.FLOAT(precision=10), nullable=False)
    location_long = db.Column(db.FLOAT(precision=10), nullable=False)
    radius = db.Column(db.Integer, nullable=False)
    is_enable = db.Column(db.Boolean, nullable=False, default=True)
    is_active = db.Column(db.Boolean, nullable=False, default=True)
    created_at = db.Column(db.DateTime, default=datetime.utcnow)
    updated_at = db.Column(db.DateTime, default=datetime.utcnow)

    def __repr__(self):
        return '<Geofence {}>'.format(self.name)
