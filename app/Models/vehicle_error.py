from app import db
from datetime import datetime
from app.Models.vehicle import Vehicle

class VehicleError(db.Model):
    __tablename__ = 'vehicle_errors'
    id = db.Column(db.BigInteger, primary_key=True, autoincrement=True)
    vcu_id = db.Column(db.String(100), db.ForeignKey('vehicles.vcu_id'))
    title = db.Column(db.String(30), nullable=True)
    code = db.Column(db.String(30), nullable=True)
    error_type = db.Column(db.String(300), nullable=True)
    is_active = db.Column(db.Boolean, nullable=False, default=True)
    created_at = db.Column(db.DateTime, default=datetime.utcnow)
    updated_at = db.Column(db.DateTime, default=datetime.utcnow)

    def __repr__(self):
        return '<VehicleError {}>'.format(self.name)
