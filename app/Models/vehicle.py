from app import db
from datetime import datetime
from app.Models.user import User
from sqlalchemy.orm import relationship, backref

class Vehicle(db.Model):
    __tablename__ = 'vehicles'
    id = db.Column(db.BigInteger, primary_key=True, autoincrement=True)
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'))
    # users = db.relationship("User", backref=db.backref("users", uselist=False))
    auth_id = db.Column(db.String(50), index=True, nullable=False)
    vcu_id = db.Column(db.String(100), unique=True, index=True, nullable=False)
    vcus_lists = relationship("VcuDetail", cascade="all,delete", backref="Vehicle")
    variant_name = db.Column(db.String(30), nullable=True, default='Infinity')
    vehicle_mode = db.Column(db.Enum('Vacation Mode', 'Active Mode', name='vehicle_modes'), nullable=False, default='Active Mode')
    register_number = db.Column(db.String(30), nullable=True)
    country_code = db.Column(db.String(100), nullable=True, default='+91')
    payment_type = db.Column(db.String(100), nullable=True)
    owner_model = db.Column(db.String(100), nullable=True)
    color = db.Column(db.String(50), nullable=True)
    subscription_model = db.Column(db.Enum('Charging Model', 'Swapping Model', name='subscription_models'), nullable=False, default='Swapping Model')
    with_battery = db.Column(db.Boolean, default=True)
    purchase_date = db.Column(db.Date, nullable=True)
    city = db.Column(db.String(250), nullable=True)
    immobilise = db.Column(db.Boolean, unique=False, default=True)
    lock_status = db.Column(db.Enum('Locked', 'Unlocked', name='lock_stat'), nullable=False, default='Unlocked')
    bike_status = db.Column(db.Enum('Active', 'Running', name='bike_stat'), nullable=True)
    geofence = db.Column(db.Boolean, default=False)
    anti_theft = db.Column(db.Boolean, default=True)
    anti_theft_disable_till = db.Column(db.DateTime, nullable=True)
    is_active = db.Column(db.Boolean, nullable=False, default=True)
    created_at = db.Column(db.DateTime, default=datetime.utcnow)
    updated_at = db.Column(db.DateTime, default=datetime.utcnow)

    def __repr__(self):
        return '<Vehicle {}>'.format(self.id)
