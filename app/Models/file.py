from app import db
from datetime import datetime
from app.Models.vehicle import Vehicle

class File(db.Model):
    __tablename__ = 'files'
    id = db.Column(db.BigInteger, primary_key=True, autoincrement=True)
    vcu_id = db.Column(db.String(100), db.ForeignKey('vehicles.vcu_id'))
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'))
    file_type = db.Column(db.String(30), nullable=True)
    file_name = db.Column(db.String(255), nullable=True)
    is_active = db.Column(db.Boolean, nullable=False, default=True)
    created_at = db.Column(db.DateTime, default=datetime.utcnow)
    updated_at = db.Column(db.DateTime, default=datetime.utcnow)

    def __repr__(self):
        return '<File {}>'.format(self.name)
