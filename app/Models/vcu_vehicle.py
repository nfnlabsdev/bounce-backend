from app import db
from datetime import datetime
from app.Models.vcu_detail import VcuDetail
from app.Models.vehicle import Vehicle

class VcuVehicle(db.Model):
    __tablename__ = 'vcu_vehicles'
    id = db.Column(db.BigInteger, primary_key=True, autoincrement=True)
    vcu_details_id = db.Column(db.Integer, db.ForeignKey('vcu_details.id'))
    vehicle_id = db.Column(db.Integer, db.ForeignKey('vehicles.id'))
    start_date = db.Column(db.Date, nullable=True)
    end_date = db.Column(db.Date, nullable=True)
    is_active = db.Column(db.Boolean, nullable=False, default=True)
    created_at = db.Column(db.DateTime, default=datetime.utcnow)
    updated_at = db.Column(db.DateTime, default=datetime.utcnow)

    def __repr__(self):
        return '<VcuVehicle {}>'.format(self.name)
