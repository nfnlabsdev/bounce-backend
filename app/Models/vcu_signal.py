from app import db
from datetime import datetime
from app.Models.vehicle import Vehicle

class VcuSignal(db.Model):
    __tablename__ = 'vcu_signals'
    id = db.Column(db.BigInteger, primary_key=True, autoincrement=True)
    vcu_id = db.Column(db.String(100), db.ForeignKey('vehicles.vcu_id'))
    refer_id = db.Column(db.String(30), nullable=True)
    sent_by = db.Column(db.String(30), nullable=True)
    code = db.Column(db.String(30), nullable=True)
    message_type = db.Column(db.String(30), nullable=True)
    description = db.Column(db.String(300), nullable=True)
    mqtt_content = db.Column(db.String(64000), nullable=True)
    is_active = db.Column(db.Boolean, nullable=False, default=True)
    created_at = db.Column(db.DateTime, default=datetime.utcnow)
    updated_at = db.Column(db.DateTime, default=datetime.utcnow)

    def __repr__(self):
        return '<VcuSignal {}>'.format(self.name)
