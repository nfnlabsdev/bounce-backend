from app import db
from datetime import datetime
from app.Models.vehicle import Vehicle

class TheftAlert(db.Model):
    __tablename__ = 'theft_alerts'
    id = db.Column(db.BigInteger, primary_key=True, autoincrement=True)
    vcu_id = db.Column(db.String(100), db.ForeignKey('vehicles.vcu_id'))
    location_lat = db.Column(db.FLOAT(precision=10), nullable=False)
    location_long = db.Column(db.FLOAT(precision=10), nullable=False)
    user_response = db.Column(db.String(100), nullable=True)
    disable_till = db.Column(db.DateTime, nullable=True)
    is_active = db.Column(db.Boolean, nullable=False, default=True)
    created_at = db.Column(db.DateTime, default=datetime.utcnow)
    updated_at = db.Column(db.DateTime, default=datetime.utcnow)

    def __repr__(self):
        return '<TheftAlert {}>'.format(self.name)
