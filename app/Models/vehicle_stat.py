from app import db
from datetime import datetime
from app.Models.vehicle import Vehicle

class VehicleStat(db.Model):
    __tablename__ = 'vehicle_stats'
    id = db.Column(db.BigInteger, primary_key=True, autoincrement=True)
    vcu_id = db.Column(db.String(100), db.ForeignKey('vehicles.vcu_id'))
    user_name = db.Column(db.String(300), nullable=True)
    engine_mode = db.Column(db.Enum('Eco Mode', 'Power Mode', name='engine_modes'), nullable=False, default='Eco Mode')
    battery_percentage = db.Column(db.Integer, nullable=True)
    remaining_range = db.Column(db.FLOAT(precision=10), nullable=True)
    Ignition_status = db.Column(db.Boolean, nullable=False, default=True)
    parked_location_lat = db.Column(db.FLOAT(precision=10), nullable=True)
    parked_location_long = db.Column(db.FLOAT(precision=10), nullable=True)
    bin_number = db.Column(db.String(300), nullable=True)
    battery_voltage = db.Column(db.String(300), nullable=True)
    battery_discharge = db.Column(db.Boolean, nullable=True)
    battery_charge = db.Column(db.Boolean, nullable=True)
    battery_connect_status = db.Column(db.Boolean, nullable=True)
    motor_brake = db.Column(db.Boolean, nullable=True)
    motor_cruise = db.Column(db.Boolean, nullable=True)
    remote_ignition = db.Column(db.Boolean, nullable=True)
    last_synced = db.Column(db.DateTime, default=datetime.utcnow)
    whatever = db.Column(db.String(300), nullable=True)
    is_active = db.Column(db.Boolean, nullable=False, default=True)
    created_at = db.Column(db.DateTime, default=datetime.utcnow)
    updated_at = db.Column(db.DateTime, default=datetime.utcnow)

    def __repr__(self):
        return '<VehicleStat {}>'.format(self.id)
