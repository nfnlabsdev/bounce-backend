from app import db
from flask import request, jsonify, make_response, session
import os
import json

from app.Models.vehicle import Vehicle
from app.Models.user import User
from app.Controller import vehicle_controller

def userDetails(vcu_id):
    vehicle_det = db.session.query(Vehicle).where(Vehicle.vcu_id == vcu_id, Vehicle.is_active == True).all()
    return vehicle_controller.vehicle_json_generate(vehicle_det)
