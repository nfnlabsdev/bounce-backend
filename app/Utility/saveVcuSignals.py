from app import db
from flask import request, jsonify, make_response, session
import os
import json

from app.Models.vehicle import Vehicle
from app.Models.vcu_signal import VcuSignal

def saveLog(code, topic, messagePayload):

    # if topic.split('/')[3] == "VCU":
    #     code = "home"
    # else:
    #     code = "other"

    save_vcu_signals = VcuSignal(vcu_id=topic.split('/')[1],
                    sent_by="vcu",
                    mqtt_content=messagePayload,
                    code=code
                    )
    db.session.add(save_vcu_signals)
    db.session.commit()
