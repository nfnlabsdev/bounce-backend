from app import db
from flask import request, jsonify, make_response, session
import os
import json

from app.Models.vehicle import Vehicle
from app.Models.user import User
from app.Controller import vehicle_controller

def userDetails(uid):
    users = db.session.query(Vehicle).where(Vehicle.auth_id == str(uid), Vehicle.is_active == True).first()
    return vehicle_controller.vehicle_json_generate([users])
