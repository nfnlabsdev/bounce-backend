from app import db
from flask import request, jsonify, make_response, session
import requests, json, os

def send_notification(no_type, geo_lat, geo_long, radius, lat, long, breach_id, id, title, description, token):
    # to_tokens = [token]
    notification_token = {
                        "data":  {
                            "title": title,
                            "message": description,
                            "customData": {"no_type": no_type, "id": id, "lat": lat, "long": long, "breach_id": breach_id, "geo_lat": geo_lat, "geo_long": geo_long, "radius": radius},
                            "sound": "ping.aiff",
                            "icon": "https://bounceinfinity.com/static/media/logo.e3b15ad0.svg",
                            "imageUrl": ""
                            },
                        "notification": {
                            "title": title,
                            "body": description,
                            "image": ""
                           },
                        "priority": "high",
                        "android": {
                            "notification": {
                                  "imageUrl": ""
                                }
                              },
                        "to": token
                        }
    # print(notification_token)
    url='https://fcm.googleapis.com/fcm/send'
    req_headers = {"Content-Type":  "application/json", "Authorization": "key=AAAAeFr8Wr8:APA91bH1seKQL7uoC3EvNaYS7uegyZLp_bcsq5jrc7XFT0ogFRd7bPsKG_xbAuvMnhPm3ym1qJg-dR5fhawGVr7C-EvT9H4RoxHZsAd6WweLXYuvjzZKWUW6V-XV8IfZ5ZINilggkU7i"}
    r = requests.post(url,headers=req_headers,data=json.dumps(notification_token).encode('utf-8'))
    user_response = r.json()
    print(user_response)
    return user_response
