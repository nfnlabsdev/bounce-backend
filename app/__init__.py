from flask import Flask, session
from config.database import Config
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.orm import relationship
from flask_migrate import Migrate
from flask_cors import CORS
from datetime import datetime
from flask_admin import Admin
from flask_admin.contrib.sqla import ModelView
import boto3, botocore
import socket
import sentry_sdk
from sentry_sdk.integrations.flask import FlaskIntegration


app = Flask(__name__)

app.config.from_object(Config)
db = SQLAlchemy(app)
migrate = Migrate(app, db)

from app.Models.vehicle_stat import VehicleStat
from app.Models.vcu_detail import VcuDetail

app.config['S3_BUCKET'] = "infinity-images-stg"
app.config['S3_KEY'] = "AKIA52DD5QA5BNDY5WDV"
app.config['S3_SECRET'] = "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"

cors = CORS(app)
admin = Admin(app, name='Bounce Infinity', template_mode='bootstrap3')
app.config['FLASK_ADMIN_SWATCH'] = 'united'

from app.Models import user, vehicle, vcu_code, vcu_detail, vcu_vehicle, app_prefernce, file, geofence, geofence_breach, vehicle_stat, theft_alert, vehicle_error, vcu_signal, dms_handshake, user_vehicle, notification_token
from routes import routes
from flask_mqtt import Mqtt
from app.Utility import saveVcuSignals

from app.Models.vehicle import Vehicle
from app.Models.user import User
from app.Models.vcu_signal import VcuSignal
from app.Models.vehicle_stat import VehicleStat
from app.Models.user_vehicle import UserVehicle
from app.Models.vcu_vehicle import VcuVehicle
from app.Models.vcu_detail import VcuDetail
from app.Models.dms_handshake import DmsHandshake
from app.Models.notification_token import NotificationToken
from app.Models.geofence import Geofence
from app.Models.geofence_breach import GeofenceBreach
from app.Models.theft_alert import TheftAlert

sentry_sdk.init(
    dsn="https://13b19e8693934257ac76b96507674098@o71721.ingest.sentry.io/6235233",
    integrations=[FlaskIntegration()],
    traces_sample_rate=1.0,
)

app.config['SECRET'] = 'my secret key'
app.config['MQTT_BROKER_URL'] = 'infinityvcu.staging.bounceinfinity.com'
app.config['MQTT_BROKER_PORT'] = 1883
app.config['MQTT_CLIENT_ID'] = 'c7231176-59ac-494b-a7c9-616a0cd7610ee'
app.config['MQTT_CLEAN_SESSION'] = True
app.config['MQTT_USERNAME'] = 'brokeruser'
app.config['MQTT_PASSWORD'] = 'hy77Hj_sxcTgasdfr#'
app.config['MQTT_KEEPALIVE'] = 5
app.config['MQTT_TLS_ENABLED'] = False
app.config['MQTT_LAST_WILL_TOPIC'] = 'test'
app.config['MQTT_LAST_WILL_MESSAGE'] = 'bye'
app.config['MQTT_LAST_WILL_QOS'] = 0
mqtt = Mqtt(app)

class AdminView(ModelView):
        can_delete = False
        can_edit = False  
        can_create = False  
        page_size = 10
        column_exclude_list = ['created_at','updated_at']
        column_filters = ['vcu_id']


class UserView(ModelView):
    can_delete = False  
    can_edit = False  
    can_create = False  
    page_size = 10
    column_exclude_list = ['created_at','updated_at']
    column_filters = ['phonenumber']

class VcuSignalView(ModelView):
        can_delete = False
        can_view_details = True
        column_searchable_list = ['vcu_id']
        column_filters = ['vcu_id']
        column_exclude_list = ['message_type','description','is_active','sent_by','created_at']
        column_editable_list = ['mqtt_content']

class GeofenceView(ModelView):
        can_delete = False
        can_create = False
        can_edit = False
        page_size = 10
        can_view_details = True
        column_searchable_list = ['name', 'radius']
        form_included_columns = ['vcu_id']
        column_filters = ['vcu_id']
        column_exclude_list = ['created_at','updated_at','is_active']

class VehicleView(ModelView):
        page_size = 10
        can_view_details = True
        can_create = False
        can_delete = False
        can_edit = False
        column_filters = ['vcu_id']
        column_exclude_list = ['created_at','updated_at']
        column_editable_list = ['vehicle_mode', 'color', 'subscription_model']
        can_create = False
        form_choices = {
            'color': [
                ('silver', 'silver'),
                ('red', 'red'),
                ('black', 'black'),
                ('white', 'white'),
                ('gray', 'gray')
            ]
        }
        can_export = True

class VehicleStatView(ModelView):
        page_size = 10
        can_create = False
        can_delete = False
        can_edit = True
        can_view_details = True
        column_filters = ['vcu_id']
        column_exclude_list = ['created_at','updated_at','is_active']
        column_editable_list = ['engine_mode', 'battery_percentage', 'remaining_range', 'parked_location_lat', 'parked_location_long','battery_connect_status','bin_number','battery_charge','battery_discharge']

admin.add_view(VehicleView(Vehicle, db.session))
admin.add_view(VehicleStatView(VehicleStat, db.session))
admin.add_view(UserView(User, db.session))
admin.add_view(VcuSignalView(VcuSignal, db.session, category="Other Tables"))
admin.add_view(GeofenceView(Geofence, db.session, category="Other Tables"))
admin.add_view(AdminView(GeofenceBreach, db.session, category="Other Tables"))
admin.add_view(AdminView(TheftAlert, db.session, category="Other Tables"))
        # base_url = request.base_url.split('login_post')[0] + 'admin/login'
        # redirect(base_url)



if __name__ == '__main__':
    app.run(port=7000, debug=true)
