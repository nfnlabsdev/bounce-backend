from sqlalchemy import *
from sqlalchemy.sql import exists
from app import db
from flask import request, jsonify, make_response, session
import os
import json
from datetime import datetime
from geopy.distance import geodesic

from app.Models.vehicle import Vehicle
from app.Models.user import User
from app.Models.geofence import Geofence
from app.Models.geofence_breach import GeofenceBreach
from app.Models.vehicle_stat import VehicleStat
from app.Models.notification_token import NotificationToken
from app.Models.theft_alert import TheftAlert

from app.Utility import fcm, detailsByVcu


def save():
    res = {}
    try:
        request_data = request.json["data"]
        vcu_id = session["vehicle_object"]["vcu_id"]
        # Check VCU_ID
        vehicle_exists = db.session.query(db.exists().where(Vehicle.vcu_id == vcu_id)).scalar()
        if vehicle_exists == False:
            res['data'] = None
            res['status'] = False
            res['message'] = "Vehicle not available"
            return make_response(jsonify(res)), 400

        #Check Geofence
        # geofence_exists = db.session.query(db.exists().where(Geofence.name == request_data["name"],Geofence.vcu_id == vcu_id)).scalar()
        geofence_exists = Geofence.query.filter_by(name=request_data["name"],vcu_id=vcu_id).first()
        if geofence_exists is not None:
            res['data'] = None
            res['status'] = False
            res['message'] = "Geofence name already exists"
            return make_response(jsonify(res)), 400

        #Adding geofence
        save_geofence = Geofence(vcu_id=vcu_id,
                        name=request_data["name"],
                        address=request_data["address"],
                        landmark=request_data["landmark"],
                        location_lat=request_data["location_lat"],
                        location_long=request_data["location_long"],
                        radius=request_data["radius"]
                        )
        db.session.add(save_geofence)
        db.session.commit()

        res['data'] = save_geofence.id
        res['status'] = True
        res['message'] = "Data added successfully !"
        return make_response(jsonify(res)), 200

    except Exception as e:
        res['data'] = None
        res['status'] = False
        res['message'] = str(e)
        return make_response(jsonify(res)), 400

def geofence_update():

    res = {}
    try:
        request_data = request.json["data"]
        vcu_id = session["vehicle_object"]["vcu_id"]
        # Check VCU_ID
        vehicle_exists = db.session.query(db.exists().where(Vehicle.vcu_id == vcu_id)).scalar()
        if vehicle_exists == False:
            res['data'] = None
            res['status'] = False
            res['message'] = "Vehicle not available"
            return make_response(jsonify(res)), 400

        #updating geofence
        geofence_obj = Geofence.query.filter_by(id=request_data["id"]).first()
        if geofence_obj is None:
            res['data'] = None
            res['status'] = False
            res['message'] = "Geofence id not available"
            return make_response(jsonify(res)), 400

        #Check Geofence
        geofence_exists = Geofence.query.filter_by(name=request_data["name"],vcu_id = vcu_id).first()
        if (geofence_exists and (geofence_exists.id != request_data["id"])):
            res['data'] = None
            res['status'] = False
            res['message'] = "Geofence name already exists"
            return make_response(jsonify(res)), 400

        #Update vehicle data
        geofence_obj.name=request_data["name"],
        geofence_obj.address=request_data["address"],
        geofence_obj.landmark=request_data["landmark"],
        geofence_obj.location_lat=request_data["location_lat"],
        geofence_obj.location_long=request_data["location_long"],
        geofence_obj.radius=request_data["radius"]
        db.session.commit()

        res['data'] = geofence_obj.id
        res['status'] = True
        res['message'] = "Data updated successfully !"
        return make_response(jsonify(res)), 200

    except Exception as e:
        res['data'] = None
        res['status'] = False
        res['message'] = str(e)
        return make_response(jsonify(res)), 400

def list():
    res = {}
    try:
        vcu_id = session["vehicle_object"]["vcu_id"]
        all_geofences = db.session.query(Geofence).filter(and_(Geofence.vcu_id.in_([vcu_id]), Geofence.is_active==True)).all()
        res['data'] = geofence_json_generate(all_geofences)
        res['status'] = True
        res['message'] = "Geofence list"
        return make_response(jsonify(res)), 200

    except Exception as e:
            res['data'] = None
            res['status'] = False
            res['message'] = str(e)
            return make_response(jsonify(res)), 400

def switch_state():
    res = {}
    request_data = request.json["data"]
    try:
        for geofenceObj in request_data:
            update_geofences = db.session.query(Geofence).where(Geofence.id == geofenceObj['id']).first()
            update_geofences.is_enable = geofenceObj['state']
            db.session.commit()

        res['data'] = request_data
        res['status'] = True
        res['message'] = "Geofence state updated"
        return make_response(jsonify(res)), 200
    except Exception as e:
        res['data'] = None
        res['status'] = False
        res['message'] = str(e)
        return make_response(jsonify(res)), 400

def switch_notifiy_state():
    res = {}
    request_data = request.json["data"]
    try:
        vcu_id = session["vehicle_object"]["vcu_id"]
        update_geofence = db.session.query(Vehicle).where(Vehicle.vcu_id == vcu_id).first()
        update_geofence.geofence = request_data['state']
        db.session.commit()

        update_breach_data = db.session.query(GeofenceBreach).filter_by(vcu_id=vcu_id).update({GeofenceBreach.is_active: False})
        db.session.commit()

        res['data'] = None
        res['status'] = True
        res['message'] = "Geofence notification state updated"
        return make_response(jsonify(res)), 200
    except Exception as e:
        res['data'] = None
        res['status'] = False
        res['message'] = str(e)
        return make_response(jsonify(res)), 400

def get_notifiy_state():
    res = {}
    try:
        vcu_id = session["vehicle_object"]["vcu_id"]
        get_geofence = db.session.query(Vehicle).where(Vehicle.vcu_id == vcu_id).first()
        res['data'] = get_geofence.geofence
        res['status'] = True
        res['message'] = "Geofence notification state"
        return make_response(jsonify(res)), 200
    except Exception as e:
        res['data'] = None
        res['status'] = False
        res['message'] = str(e)
        return make_response(jsonify(res)), 400

def anti_theft_notification(lat, lon, vcu_id):

    res = {}
    vehicle_obj = Vehicle.query.filter_by(vehicle_mode="Active Mode", vcu_id=vcu_id, anti_theft=True, is_active=True).first()
    print("vehicle", vehicle_obj)
    if vehicle_obj is None:
        res['data'] = None
        res['status'] = False
        res['message'] = "Notification turned off / anti_theft"
        print(res['message'])
        return res

    fcm_notification_tokens = NotificationToken.query.filter_by(vcu_id=vcu_id, is_active=True).first()
    print("notification token", fcm_notification_tokens.token_str)
    if fcm_notification_tokens is None:
        res['data'] = None
        res['status'] = False
        res['message'] = "Notification token not available"
        print(res['message'])
        return res

    #Adding theft
    save_theft = TheftAlert(vcu_id=vcu_id,
                    location_lat=lat,
                    location_long=lon
                    )
    db.session.add(save_theft)
    db.session.commit()


    print("anti-theft-notification", True)
    send_notificaion = fcm.send_notification(2, '', '','',lat, lon, save_theft.id, None, "Your scooter detected an Anti Theft alert", "Your scooter is undergoing some signs of anti theft. The scooter’s rear wheel will be locked for an hour.", fcm_notification_tokens.token_str)
    return send_notificaion

def find_geofence_breach(vehicle_current_lat,vehicle_current_long,vcu_id):
    res = {}
    try:
        geo_list = []

        vehicle_obj = Vehicle.query.filter_by(vehicle_mode="Active Mode", vcu_id=vcu_id, geofence=True, is_active=True).first()
        print("vehicle", vehicle_obj)
        if vehicle_obj is None:
            res['data'] = None
            res['status'] = False
            res['message'] = "Notification turned off / geofence"
            print(res['message'])
            return res

        fcm_notification_tokens = NotificationToken.query.filter_by(vcu_id=vcu_id, is_active=True).first()
        print("notification token", fcm_notification_tokens.token_str)
        if fcm_notification_tokens is None:
            res['data'] = None
            res['status'] = False
            res['message'] = "Notification token not available"
            print(res['message'])
            return res

        breached_geofences = db.session.query(Geofence).where(Geofence.is_enable == True, Geofence.vcu_id == vcu_id).all()
        print("breached_geofences", breached_geofences)
        if breached_geofences is None:
            res['data'] = None
            res['status'] = False
            res['message'] = "Braches not available"
            print(res['message'])
            return res


        breached_geofences = geofence_json_generate(breached_geofences)

        if len(breached_geofences) == 0:
            # sql = 'update geofence_breaches set is_active = false where vcu_id = ' + vcu_id
            # vehicle_obj_update = db.engine.execute(str(sql))
            update_breach_data = db.session.query(GeofenceBreach).filter_by(vcu_id=vcu_id).update({GeofenceBreach.is_active: False})
            db.session.commit()

            res['data'] = None
            res['status'] = False
            res['message'] = "breached_geofences not found"

        for geofenceObj in breached_geofences:
            resObj = {}
            vechicle_location = (vehicle_current_lat, vehicle_current_long)
            setting_location = (geofenceObj["location_lat"], geofenceObj["location_long"])
            distance = geodesic(vechicle_location, setting_location).km
            resObj['geofence_id'] = geofenceObj["id"]
            resObj['name'] = geofenceObj["name"]
            resObj['distance'] = distance
            resObj['radius'] = geofenceObj["radius"]/1000
            geo_list.append(resObj)

            if (resObj['radius'] < resObj['distance']):
                print("geofence", resObj['name'])
                print(distance)
                save_geofence_breach = GeofenceBreach(vcu_id=geofenceObj["vcu_id"],
                                geofence_id=geofenceObj["id"],
                                location_lat=vehicle_current_lat,
                                location_long=vehicle_current_long
                                )
                db.session.add(save_geofence_breach)
                db.session.commit()
                print('vehicle_objregister_number', vehicle_obj.register_number)
                # notification_content = "Informing you that your bike " + vcu_id + " was out of your " + resObj['name']
                notification_content = "Informing you that your bike " + vehicle_obj.register_number + " was out of your " + resObj['name']
                print("notification_content", notification_content)
                # vehicle_data = VehicleStat.query.filter_by(vcu_id=vcu_id).first()
                send_notificaion = fcm.send_notification(1, geofenceObj["location_lat"], geofenceObj["location_long"],geofenceObj["radius"],vehicle_current_lat, vehicle_current_long, save_geofence_breach.id, geofenceObj["id"], "Geofence Breached", notification_content, fcm_notification_tokens.token_str)
                geo_list.append(save_geofence_breach)

        res['data'] = geo_list
        res['status'] = True
        res['message'] = "List of Geofence breaches"
        return res

    except Exception as e:
        res['data'] = None
        res['status'] = False
        res['message'] = str(e)
        print(res['message'])
        return res

def get_breached_geofences():
    res = {}
    try:
        if session["vehicle_object"]["geofence"] == False:
            res['data'] = None
            res['status'] = False
            res['message'] = "Notification turned off"
            return make_response(jsonify(res)), 400

        vcu_id = session["vehicle_object"]["vcu_id"]
        breached_geofences = db.session.execute(
    "select DISTINCT ON (id) g.id as id, g.name, g.vcu_id, g.address, g.landmark, gb.location_lat, gb.location_long, g.is_enable, gb.created_at, g.radius from geofence_breaches as gb LEFT JOIN geofences as g ON gb.geofence_id=g.id where gb.vcu_id = :vcu and g.is_enable = true and gb.is_active = true",
    {'vcu': vcu_id})

        res['data'] = geofence_json_generate(breached_geofences)
        res['status'] = True
        res['message'] = "Breached Geofence list"
        return make_response(jsonify(res)), 200

    except Exception as e:
            res['data'] = None
            res['status'] = False
            res['message'] = str(e)
            return make_response(jsonify(res)), 400

def geofence_user_response():
    res = {}
    try:
        request_data = request.json["data"]
        geofence_breach_obj = GeofenceBreach.query.filter_by(id=request_data['id']).first()
        geofence_breach_obj.user_response=request_data["user_response"]
        db.session.commit()
        res['data'] = geofence_breach_obj.user_response
        res['status'] = True
        res['message'] = "Geofence response added"
        return make_response(jsonify(res)), 200

    except Exception as e:
            res['data'] = None
            res['status'] = False
            res['message'] = str(e)
            return make_response(jsonify(res)), 400

def anti_theft_user_response():
    res = {}
    try:
        request_data = request.json["data"]
        vcu_id = session["vehicle_object"]["vcu_id"]
        anti_theft_obj = TheftAlert.query.filter_by(id=request_data['id']).first()
        anti_theft_obj.user_response=request_data["user_response"]
        db.session.commit()
        res['data'] = anti_theft_obj.user_response
        res['status'] = True
        res['message'] = "Anti theft response added"
        return make_response(jsonify(res)), 200

    except Exception as e:
            res['data'] = None
            res['status'] = False
            res['message'] = str(e)
            return make_response(jsonify(res)), 400

def geofence_json_generate(values):
    return [
        {"id": i.id, "created_at": i.created_at, "name": i.name, "address": i.address, "landmark": i.landmark, "vcu_id": i.vcu_id, "location_lat": i.location_lat, "location_long": i.location_long, "radius": i.radius,"is_enable": i.is_enable}
        for i in values
    ]

def geofence_breach_json_generate(values):
    return [
        {"geofence_id": i.geofence_id, "user_response": i.user_response, "address": i.address, "landmark": i.landmark, "landmark": i.landmark, "location_lat": i.location_lat, "location_long": i.location_long}
        for i in values
    ]

def find_distance(from_location, to_location):
    geodesic(from_location, to_location).kms
