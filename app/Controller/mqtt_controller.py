from flask_mqtt import Mqtt
from random import randrange, uniform
import time
from flask import Flask, request, jsonify, make_response, session
from config.database import Config
from sqlalchemy import *
from sqlalchemy.sql import exists
from app import db
import json
import datetime
import requests

from app.Models.vcu_signal import VcuSignal
from app.Models.vehicle import Vehicle
from app.Models.vehicle_stat import VehicleStat
from app.Models.theft_alert import TheftAlert
from app.Models.vehicle_error import VehicleError

from app.Utility import fcm, detailsByVcu, detailsByuid, saveVcuSignals
from app.Controller import vehicle_controller, user_controller, geofence_controller



app = Flask(__name__)
app.config.from_object(Config)
app.config['SECRET'] = 'my secret key'
app.config['MQTT_BROKER_URL'] = 'infinityvcu.staging.bounceinfinity.com'
app.config['MQTT_BROKER_PORT'] = 1883
app.config['MQTT_CLIENT_ID'] = 'c7231176-59ac-494b-a7c9-616a0cd7610ee'
app.config['MQTT_CLEAN_SESSION'] = True
app.config['MQTT_USERNAME'] = 'brokeruser'
app.config['MQTT_PASSWORD'] = 'hy77Hj_sxcTgasdfr#'
app.config['MQTT_KEEPALIVE'] = 100
app.config['MQTT_TLS_ENABLED'] = False
app.config['MQTT_LAST_WILL_TOPIC'] = 'test'
app.config['MQTT_LAST_WILL_MESSAGE'] = 'bye'
app.config['MQTT_LAST_WILL_QOS'] = 2
mqtt = Mqtt(app)

def payload_gen():
    return "{'Scuter_Id':'67543','DDMMYY':'061221','hhmmss':'184420','Fw_ver':'184420','Hw_ver':'184420','gsm_sgnl_qual':21,'gps_lat_time':'180010','gps_latch_sts':'TRUE','gps_Lat':" + str(uniform(12.082680, 13.082680)) + ",'gps_long':" + str(uniform(79.270721, 80.270721)) + ",'gps_acc':265,'gps_sat':23,'gps_heading':NULL,'accX':1029,'accY':21,'accZ':1030,'angX':2110,'angY':1029,'angZ':3029,'Motor_speed': " + str(uniform(20, 60)) + ",'Veh_Mode': " + str(uniform(20, 60)) + ",'VCU_DO':32,'VCU_DI':12,'Sys_Err':300,'Sys_Sts':500,'ODO':" + str(randrange(1200, 1500)) + ",'Range': " + str(randrange(15, 30)) + ",'SOC':32,'SOH':98,'batt_voltage': " + str(uniform(20000, 60000)) + ",'batt_current':-16,'batt_temp': " + str(uniform(15, 30)) + ",'batt_err':0}"

@mqtt.on_connect()
def handle_connect(client, userdata, flags, rc):
    # mqtt.subscribe("BOUNCE/685GS3sU/data/VCU")
    # mqtt.subscribe("BOUNCE/685GS3222U/data/VCU")
    # mqtt_publish_config('63673989')
    subcribe_to_vcu()

@mqtt.on_message()
def handle_message(client, userdata, message):
    try:
        vcu_id=message.topic.split('/')[1]
        vehicle_obj = Vehicle.query.filter_by(vcu_id=vcu_id, is_active=True).first()
        # vehicle_obj = Vehicle.query.filter_by(vehicle_mode="Active Mode", vcu_id=vcu_id, is_active=True).first()
        if vehicle_obj is None:
            print("Vehicle not available")
            return False

        print("vehicle_obj", vehicle_obj)
        if vehicle_obj.vehicle_mode == "Active Mode":
            if message.topic.split('/')[2] == 'Data':
                print('home page')
                mqttdecode(message.payload.decode("utf-8"),message.topic.split('/')[1])
                saveVcuSignals.saveLog("home", message.topic, str(message.payload.decode("utf-8")))
            else:
                mqttdecode_cntl(message.payload.decode("utf-8"),message.topic.split('/')[1])
                saveVcuSignals.saveLog("config", message.topic, str(message.payload.decode("utf-8")))
        else:
            print("Scooter in Vacation mode")
            if message.topic.split('/')[2] == 'Data':
                mqtt_data = json.loads(message.payload.decode("utf-8"))
                if (mqtt_data['VH_Mod'] == 1):
                    mqttdecode(message.payload.decode("utf-8"),message.topic.split('/')[1])
                    saveVcuSignals.saveLog("home", message.topic, str(message.payload.decode("utf-8")))
    except Exception as e:
        print("mqtt-exception-json", message.payload.decode("utf-8"))
        print("mqtt-exception-error", e)

def mqttdecode(message,vcu_id):
    print("from",vcu_id)
    vcu_di_data_array = ['Ignition I/P','Head Lamp I/P','Hazard I/P','Right Winker I/P','Left Winker I/P','Cruise I/P','Side Stand I/P','Power Mode I/P','Eco Mode I/P','Brake I/P','Key I/P','Reserved','Charger I/P','External Pwr I/P','Reserved1','Reserved2']
    vcu_do_data_array = ['Reverse O/P','Cruise O/P','48V O/P','Left Winker O/P','Right Winker O/P','Reserved','High Beam O/P','Power Mode O/P','Drag Mode O/P','Anti-theft O/P','SideStand O/P','Acc_Key O/P','Charger O/P','External Power O/P','Reserved1','Reserved2']
    sys_err_data_array = ['Throttle Fault','Brake Fault','MotorHall Fault','ECU Fault','Motor UnderVolt','Motor OverVolt','Motor OverTemp','Motor OverCurr','Battery Error']
    sys_sts_data_array =['Motor Reverse','Motor Cruise','Motor Startup Lock','Motor Brake','Battery Discharge','Battery Charge','Battery Connect Status','Anti Theft Alert']
    vcu_cntrl_data_array = ['Immobilize','Vehicle Mode','Remote Ignition','FOTA','FindMe']
    print("message",message)
    mqtt_data = json.loads(message)
    # text = input(mqtt_data['HMS'])
    # decoded = ' '.join(text[::3])
    last_updated = "20" + mqtt_data['DMY'][4] + mqtt_data['DMY'][5] + "-" + mqtt_data['DMY'][2] + mqtt_data['DMY'][3] + "-" + mqtt_data['DMY'][0] + mqtt_data['DMY'][1] + " " + mqtt_data['HMS'][0] + mqtt_data['HMS'][1] + ":" + mqtt_data['HMS'][2] + mqtt_data['HMS'][3] + ":" + mqtt_data['HMS'][4] + mqtt_data['HMS'][5]

    check_brach = geofence_controller.find_geofence_breach(mqtt_data['GPS_Lat'], mqtt_data['GPS_Lon'], vcu_id)
    vcu_di_data = match_data(vcu_di_data_array,list(format(int(mqtt_data['VCU_DI']), "016b"))[::-1])
    vcu_do_data = match_data(vcu_do_data_array,list(format(int(mqtt_data['VCU_DO']) , "016b"))[::-1])
    sys_err = match_data(sys_err_data_array,list(format(int(mqtt_data['SYS_Err']), "016b"))[::-1])
    sys_sts = match_data(sys_sts_data_array,list(format(int(mqtt_data['SYS_Sts']), "016b"))[::-1])
    print("sys_sts",sys_sts)
    # vcu_cntrl = match_data(vcu_cntrl_data_array,list(format(int(mqtt_data['VCU_CNTRL']), "016b"))[::-1])
    # update_vehicle=Vehicle.query.filter_by(vcu_id=vcu_id).update(dict(vehicle_mode='Active Mode' if mqtt_data['VH_Mod'] else 'Vacation Mode',immobilise=vcu_cntrl['Immobilize'],anti_theft=vcu_do_data['Anti-theft O/P']))
    update_vehicle=Vehicle.query.filter_by(vcu_id=vcu_id).update(dict(vehicle_mode='Active Mode' if mqtt_data['VH_Mod'] else 'Vacation Mode',anti_theft=vcu_do_data['Anti-theft O/P']))
    update_state = VehicleStat.query.filter_by(vcu_id = vcu_id).update(dict(engine_mode= 'Power Mode' if vcu_do_data['Power Mode O/P'] else 'Eco Mode',battery_percentage=mqtt_data['SOC'],remaining_range=mqtt_data['Range'],Ignition_status=vcu_di_data['Ignition I/P'],parked_location_lat=mqtt_data['GPS_Lat'],parked_location_long=mqtt_data['GPS_Lon'],bin_number=mqtt_data['BIN'],battery_voltage=mqtt_data['Bat_V'],battery_discharge=sys_sts['Battery Discharge'],battery_charge=sys_sts['Battery Charge'],battery_connect_status=sys_sts['Battery Connect Status'],motor_brake=sys_sts['Motor Brake'],motor_cruise=sys_sts['Motor Cruise'],last_synced=last_updated))
    if (sys_sts['Anti Theft Alert']):
        send_anti_theft = geofence_controller.anti_theft_notification(mqtt_data['GPS_Lat'], mqtt_data['GPS_Lon'], vcu_id)

    print("sys_sts_data_array",sys_sts)
    # print("time", "20" + mqtt_data['DMY'][4] + mqtt_data['DMY'][5] + "-" + mqtt_data['DMY'][2] + mqtt_data['DMY'][3] + "-" + mqtt_data['DMY'][0] + mqtt_data['DMY'][1] + " " + mqtt_data['HMS'][0] + mqtt_data['HMS'][1] + ":" + mqtt_data['HMS'][2] + mqtt_data['HMS'][3] + ":" + mqtt_data['HMS'][4] + mqtt_data['HMS'][5])
    update_error_data = db.session.query(VehicleError).filter_by(vcu_id=vcu_id).update({VehicleError.is_active: False})
    db.session.commit()

    for i in range(len(sys_err_data_array)):
        if(sys_err[sys_err_data_array[i]]):
            error_save = VehicleError(vcu_id=vcu_id,title=sys_err_data_array[i])
            db.session.add(error_save)
    db.session.commit()
    return True
    # vehicle_state_insert = VehicleStat()


def mqttdecode_cntl(message,vcu_id):
    mqtt_data = json.loads(message)
    print(int(mqtt_data['VCU_Cntrl']))
    vcu_cntrl_data_array = ['Immobilize','Vehicle Mode','Remote Ignition','FOTA','FindMe']
    vcu_cntrl = match_data(vcu_cntrl_data_array,list(format(int(mqtt_data['VCU_Cntrl']), "016b"))[::-1])
    print (vcu_cntrl)
    update_vehicle=Vehicle.query.filter_by(vcu_id=vcu_id).update(dict(vehicle_mode='Active Mode' if vcu_cntrl['Vehicle Mode'] else 'Vacation Mode',anti_theft=mqtt_data["Anti-theft"][1],immobilise=vcu_cntrl['Immobilize']))
    update_state = VehicleStat.query.filter_by(vcu_id = vcu_id).update(dict(remote_ignition=vcu_cntrl['Remote Ignition']))
    db.session.commit()
    return True

# @mqtt.on_disconnect()
# def handle_disconnect(client, userdata, rc):
#     print("Disconnected from broker")

@mqtt.on_log()
def handle_logging(client, userdata, level, buf):
    if level == MQTT_LOG_ERR:
        print('Error: {}'.format(buf))

def flask_mqtt_publish():
    a_list = list(range(1, int(request.form.get('seconds'))+1))
    for x in a_list:
        print(x)
        time.sleep(1);
        randNumber = uniform(30, 60)
        publish_data = mqtt.publish(request.form.get('topic'), payload_gen(), 1)
        print("Just published " + str(randNumber) + " to Topic test")

    res = {};
    res['msg'] = "Data feed finished"
    return make_response(jsonify(res)), 200

def home():
    res = {};
    try:
        vcu_id = session["vehicle_object"]["vcu_id"]
        vehicle_data = VehicleStat.query.filter_by(vcu_id=vcu_id).first()

        anti_theft = False
        theft_data = TheftAlert.query.filter_by(vcu_id=vcu_id, is_active=True).first()
        if theft_data is not None:
            anti_theft = True

        vehicle_err_obj = VehicleError.query.distinct(VehicleError.title).filter_by(is_active=True,vcu_id=vcu_id).count()

        home_obj = stat_json_generate([vehicle_data])[0]
        if not home_obj["battery_connect_status"]:
            token = request.headers.get("infinityToken")
            print("token da", token)
            r = requests.get('https://appapi.staging.bounce.bike/api/app/baas/battery_details', headers={'token': token})
            
            user_response = r.json()['data']
            if user_response:
                vehicle_data.battery_percentage = user_response["battery_details"]["soc"]
                vehicle_data.remaining_range = (user_response["battery_details"]["soc"]*0.8)
                # vehicle_data.parked_location_lat = user_response["battery_details"]["lat"]
                # vehicle_data.parked_location_long = user_response["battery_details"]["lon"]
                vehicle_data.bin_number = user_response["battery_details"]["battery_id"]
                db.session.commit()

                home_obj["battery_percentage"] = user_response["battery_details"]["soc"]
                home_obj["remaining_range"] = (user_response["battery_details"]["soc"]*0.8)
                # home_obj["parked_location_lat"] = user_response["battery_details"]["lat"]
                # home_obj["parked_location_long"] = user_response["battery_details"]["lon"]
                home_obj["bin_number"] = user_response["battery_details"]["battery_id"]
                home_obj["battery_full_in"] = ((100 - user_response["battery_details"]["soc"])*2)*60
                if user_response["battery_details"]["status"] == 4:
                    home_obj["charger_connect_status"] = True

        res['data'] = {"errors": vehicle_err_obj, "anti_theft": anti_theft, "home": home_obj, "vehicle": session["vehicle_object"]}
        res['status'] = True
        return make_response(jsonify(res)), 200
    except Exception as e:
        res['data'] = None
        res['status'] = False
        res['message'] = str(e)
        return make_response(jsonify(res)), 400

def vehicle_location():
    res = {}
    try:
        vcu_id = session["vehicle_object"]["vcu_id"]
        vehicle_obj = VehicleStat.query.filter_by(vcu_id=vcu_id).first()
        # res['data'] = {"last_synced": vehicle_obj.last_synced - datetime.timedelta(minutes = 330), "lat": "13.070" + str(random.randint(100, 999)), "long": "80.270" + str(random.randint(100, 999))}
        res['data'] = {"last_synced": vehicle_obj.last_synced - datetime.timedelta(minutes = 330), "lat": str(vehicle_obj.parked_location_lat), "long": str(vehicle_obj.parked_location_long)}
        res['status'] = True
        return make_response(jsonify(res)), 200
    except Exception as e:
        res['status'] = False
        res['message'] = str(e)
        return make_response(jsonify(res)), 400

def latestVehicleStatus():
    res = {};
    try:
        vcu_id = session["vehicle_object"]["vcu_id"]
        # vehicle_data = db.session.query(VcuSignal).filter(and_(VcuSignal.vcu_id.in_([vcu_id]))).order_by(VcuSignal.id.desc()).first()
        vehicle_data = VcuSignal.query.filter_by(vcu_id=vcu_id, code="home").order_by(VcuSignal.id.desc()).first()
        # send_notificaion = fcm.send_notification("vcu_id", "Test From Ravi. Geofence Breached", "Informing you that your bike TN60R 9289 was out of your geofence1", session["user_object"]["gcm_token"])
        if vehicle_data is not None:
            res = {};
            res['status'] = True
            res['data'] = vcu_json_generate([vehicle_data])
            res['user'] = session["user_object"]
            # res['whatever'] = send_notificaion
            return make_response(jsonify(res)), 200
        else:
            res = {};
            res['data'] = None
            res['status'] = False
            res['message'] = "Bike details not available"
            res['user'] = session["user_object"]
            # res['whatever'] = send_notificaion
            return make_response(jsonify(res)), 400
    except Exception as e:
        res['data'] = None
        res['status'] = False
        res['message'] = str(e)
        return make_response(jsonify(res)), 400




def mqtt_publish_values():
    res = {};
    # try:
        # publish_data = mqtt.publish(request.form.get('topic'), payload_gen(), 1)
    request_data = request.json["data"]
    vcu_id = session["vehicle_object"]["vcu_id"]
    vehicle_obj = Vehicle.query.filter_by(vcu_id=vcu_id, is_active=True).first()

    if request_data["update_type"] == 1:
        vehicle_obj.anti_theft=request_data["update_value"]
        now_value = datetime.datetime.now()
        print(now_value)

        update_theft_data = db.session.query(TheftAlert).filter_by(vcu_id=vcu_id).update({TheftAlert.is_active: False})
        db.session.commit()

        if request_data["till_time"] is not None:
            add_minutes = (int(request_data["till_time"])+19800)/60
            vehicle_obj.anti_theft_disable_till=now_value + datetime.timedelta(minutes = add_minutes)
        else:
            vehicle_obj.anti_theft_disable_till=None

        db.session.commit()
        # publish_data = mqtt.publish(request.form.get('topic'), payload_gen(), 1)

    elif request_data["update_type"] == 2:
        print("immobilise")

        update_theft_data = db.session.query(TheftAlert).filter_by(vcu_id=vcu_id).update({TheftAlert.is_active: False})
        db.session.commit()

        vehicle_obj.immobilise=request_data["update_value"]
        db.session.commit()
        # publish_data = mqtt.publish(request.form.get('topic'), payload_gen(), 1)

    elif request_data["update_type"] == 3:
        print("vachicle mode")
        if request_data["update_value"] == True:
            vehicle_obj.vehicle_mode="Active Mode"
        else:
            vehicle_obj.vehicle_mode="Vacation Mode"

        db.session.commit()
        # publish_data = mqtt.publish(request.form.get('topic'), payload_gen(), 1)

    res["status"] = True
    res['message'] = "Data updated"
    # res["data"] = vcu_id
    res["data"] = vehicle_controller.vehicle_json_generate([vehicle_obj])
    return make_response(jsonify(res)), 200
    # except Exception as e:
    #     res['data'] = None
    #     res['status'] = False
    #     res['message'] = str(e)
    #     return make_response(jsonify(res)), 400

def vcu_json_generate(values):
    return [
        {"vcu_id": i.vcu_id, "mqtt_content": json.loads(i.mqtt_content), "created_at": i.created_at, "code": None}
        for i in values ]
def stat_json_generate(values):
    return [
        {"user_name": i.user_name,
         "engine_mode": i.engine_mode,
         "battery_percentage": i.battery_percentage,
         "remaining_range": i.remaining_range,"Ignition_status": i.Ignition_status,
         "parked_location_lat": i.parked_location_lat,"parked_location_long": i.parked_location_long,
         "last_synced": i.last_synced,
         # "last_synced": i.last_synced + datetime.timedelta(minutes = 330),
         "whatever": i.whatever,
         "bin_number": i.bin_number,
         "battery_voltage": i.battery_voltage,
         "battery_discharge,": i.battery_discharge,
         "battery_charge": i.battery_charge,
         "battery_connect_status": i.battery_connect_status,
         "motor_brake": i.motor_brake,
         "motor_cruise": i.motor_cruise,
         "remote_ignition": i.remote_ignition,
         "battery_low_percentage": 10,
         "battery_full_in": 0,
         "charger_connect_status": False
         }
        for i in values ]
def match_data(a,b):
    obj =  {}
    for i in range(len(a)):
        obj[a[i]]= int(b[i])
    return obj


def subcribe_to_vcu():
    all_vehicle = Vehicle.query.filter_by(is_active=True).all()
    for i in all_vehicle:
        topic = "BOUNCE/"+i.vcu_id+"/Data/VCU"
        topic_control = "BOUNCE/"+i.vcu_id+"/Config/GET"
        print ("topic to subscribe",topic)
        print ("topic to subscribe Controller",topic_control)
        mqtt.subscribe(topic)
        mqtt.subscribe(topic_control)
    return True

@app.route('/app/ring', methods=['GET'])
def ring_scooter():
    vcu_id = session["vehicle_object"]["vcu_id"]
    res = {}
    res["status"] = True
    res['message'] = "Ring triggered in scooter"
    return make_response(jsonify(res)), 200

def new_user_subscribe(vcu_id):
    topic = "BOUNCE/"+vcu_id+"/Data/VCU"
    topic_control = "BOUNCE/"+vcu_id+"/Config/GET"
    print ("topic to subscribe",topic)
    print ("topic to subscribe Controller",topic_control)
    mqtt.subscribe(topic)
    mqtt.subscribe(topic_control)
    return {"message":"subscribe to "+vcu_id+" as new user"}

def unsubscibe_to_vcu(vcu_id):
    topic = "BOUNCE/"+vcu_id+"/Data/VCU"
    topic_control = "BOUNCE/"+vcu_id+"/Config/GET"
    print ("topic to unsubscribe",topic)
    print ("topic to unsubscribe Controller",topic_control)
    mqtt.unsubscribe(topic)
    mqtt.unsubscribe(topic_control)
    return {"message":"unsubscribe to "+vcu_id+" as new user"}


def mqtt_connections(topic):
    res = {}
    print(topic)
    mqtt.subscribe(topic)
    print('connected mqtt')
    res['data'] = None
    res['status'] = False
    res['message'] = "subscribe"+topic
    return make_response(jsonify(res)), 200




def mqtt_publish_config(vcu_id):
    vehicle_obj = Vehicle.query.filter_by(vcu_id=vcu_id).first()
    vehicle_stat_obj = VehicleStat.query.filter_by(vcu_id=vcu_id).first()
    find_me = 0
    FOTA = 0
    if vehicle_obj:
        if vehicle_stat_obj:
            obj={}
            cntrl = []
            cntrl.append(find_me)
            cntrl.append(FOTA)
            cntrl.append(0 if vehicle_stat_obj.Ignition_status==False else 1)
            cntrl.append(0 if vehicle_obj.vehicle_mode =='Vacation Mode' else 1)
            cntrl.append(0 if vehicle_obj.immobilise == False else 1)
            cntrl_bin_ = ''.join(str(e) for e in cntrl)
            obj['Server'] = ["infinityvcu.staging.bounceinfinity.com",1883,1]
            obj['Anti-theft'] = [50,vehicle_obj.anti_theft]
            obj['Speed_Lmt'] = [60,1]
            obj['Drive_Hom'] = [10,1]
            obj['Fall_Det'] = [10,1]
            obj['VCU_Cntrl']= int(cntrl_bin_,2)
            topic =  "BOUNCE/"+vcu_id+"/Config/SET"
            publish_data = mqtt.publish(topic, str(obj), 1)
        else:
            print("one")
    else:
        print("two")
    return True
