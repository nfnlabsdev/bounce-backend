from app import *
from flask import request, jsonify, make_response, session
import boto3, botocore
from werkzeug.utils import secure_filename
import random
from datetime import datetime, timedelta

from app.Utility import fcm
from app.Controller import vehicle_controller, user_controller, geofence_controller

from app.Models.vehicle import Vehicle
from app.Models.user_vehicle import UserVehicle
from app.Models.user import User
from app.Models.theft_alert import TheftAlert
from app.Models.vcu_vehicle import VcuVehicle
from app.Models.vcu_detail import VcuDetail
from app.Models.dms_handshake import DmsHandshake
from app.Models.notification_token import NotificationToken
from app.Models.vehicle_stat import VehicleStat
from app.Models.vehicle_error import VehicleError



def lists():
    res = {}
    try:
        datas = Users.query.all()
        data = generate(datas)
        res['data'] = datas
        res['msg'] = "Data found!"
        return make_response(jsonify(res)), 200
    except Exception as e:
        res['data'] = None
        res['msg'] = str(e)
        return make_response(jsonify(res)), 400

def save():
    res = {}
    try:
        first_name = request.form.get('first_name')
        last_name = request.form.get('last_name')
        phonenumber = request.form.get('phonenumber')

        data = [{'first_name': first_name, 'last_name': last_name, 'phonenumber': phonenumber}]

        save = Users(first_name=first_name, last_name=last_name, phonenumber=phonenumber)
        db.session.add(save)
        db.session.commit()

        res['data'] = data
        res['msg'] = "Data added successfully !"
        return make_response(jsonify(res)), 200

    except Exception as e:
        res['data'] = None
        res['msg'] = str(e)
        return make_response(jsonify(res)), 400

def profile_update():
    res = {}
    # try:

    user_id = session["vehicle_object"]["user_id"]
    user_obj = User.query.filter_by(id=user_id).first()
    if "first_name" in request.form:
        user_obj.first_name = request.form.get('first_name')

    if "last_name" in request.form:
        user_obj.last_name = request.form.get('last_name')

    if "image" in request.files:
        file_response = image_upload(request.files["image"],user_id)
        print(file_response)
        if file_response['status'] == True:
            file_name = "https://infinity-images-stg.bounceinfinity.com/" + file_response['file_name']
            user_obj.image = file_name
        else:
            res['status'] = False
            res['message'] = "Image upload failed - " + file_response['message']
            return make_response(jsonify(res)), 400
    else:
        file_response = user_obj.image

    db.session.commit()
    res['data'] = {"image": user_obj.image, "id": user_obj.id, "first_name": user_obj.first_name, "last_name": user_obj.last_name, "phonenumber": user_obj.phonenumber}
    res['vehicle_data'] = session["vehicle_object"]
    res['status'] = True
    return make_response(jsonify(res)), 200

    # except Exception as e:
    #     res['status'] = False
    #     res['message'] = str(e)
    #     return make_response(jsonify(res)), 400

def get_profile():
    res = {}
    try:
        user_id = session["vehicle_object"]["user_id"]
        user_obj = User.query.filter_by(id=user_id).first()
        res['data'] = {"vehicle": session["vehicle_object"], "image": user_obj.image, "id": user_obj.id, "first_name": user_obj.first_name, "last_name": user_obj.last_name, "phonenumber": user_obj.phonenumber}
        res['status'] = True
        return make_response(jsonify(res)), 200
    except Exception as e:
        res['status'] = False
        res['message'] = str(e)
        return make_response(jsonify(res)), 400

def image_upload(file,id):
    res = {}
    # try:
    s3 = boto3.client(
       "s3",
       aws_access_key_id=app.config['S3_KEY'],
       aws_secret_access_key=app.config['S3_SECRET']
    )

    if file.filename == "":
        res['status'] = False
        res['message'] = "Please select a file"
        return res

    if file:
        file.filename = secure_filename("profile_" + str(id) + "_" + file.filename)
        s3.upload_fileobj(
            file,
            "infinity-images-stg.bounceinfinity.com",
            file.filename,
            ExtraArgs={
                "ACL": "public-read",
                "ContentType": file.content_type
            }
        )
        # return "infinity-images-stg.bounceinfinity.com/" + file.filename
        res['status'] = True
        res['file_name'] = file.filename
        return res
    else:
        res['status'] = False
        res['message'] = str(e)
        return res

    # except Exception as e:
    #     res['status'] = False
    #     res['msg'] = str(e)
    #     return res


def scheduled_anti_theft():
    res = {}
    # sql = 'update vehicles set anti_theft = true, anti_theft_disable_till = null where anti_theft_disable_till < CURRENT_TIMESTAMP'
    print("current_time",datetime.now() + timedelta(hours=5, minutes=30))
    update_breach_data = db.session.query(Vehicle).filter(Vehicle.anti_theft_disable_till <= datetime.now() + timedelta(hours=5, minutes=30)).update({Vehicle.anti_theft: True, Vehicle.anti_theft_disable_till: None})
    db.session.commit()
    # vehicle_obj_update = db.engine.execute(str(sql))
    res['data'] = "vehicle_obj_update"
    res['status'] = True
    return make_response(jsonify(res)), 200

def sample():
    send_notificaion = fcm.send_notification(1, '48.23456', '13.30232',350,'48.23456', '2.30232', 5, request.form.get('id'), "Test From Ravi. Geofence Breached", "Informing you that your bike TN60R 9289 was out of your geofence1", request.form.get('fcm_token'))
    return make_response(jsonify(send_notificaion)), 200

def sample2():

    res = {}
    vcu_id = request.form.get('vcu_id')
    vehicle_obj = Vehicle.query.filter_by(vehicle_mode="Active Mode", vcu_id=vcu_id, anti_theft=True, is_active=True).first()
    if vehicle_obj is None:
        res['data'] = None
        res['status'] = False
        res['message'] = "Notification turned off"
        print(res['message'])
        return res

    fcm_notification_tokens = NotificationToken.query.filter_by(vcu_id=vcu_id, is_active=True).first()
    print("notification token", fcm_notification_tokens.token_str)
    if fcm_notification_tokens is None:
        res['data'] = None
        res['status'] = False
        res['message'] = "Notification token not available"
        print(res['message'])
        return res

    #Adding theft
    save_theft = TheftAlert(vcu_id=vcu_id,
                    location_lat='48.23456',
                    location_long='2.30232'
                    )
    db.session.add(save_theft)
    db.session.commit()
    send_notificaion = fcm.send_notification(2, '', '','','48.23456', '2.30232', save_theft.id, None, "Your scooter detected an Anti Theft alert", "Your scooter is undergoing some signs of anti theft. The scooter’s rear wheel will be locked for an hour.", fcm_notification_tokens.token_str)
    return make_response(jsonify(send_notificaion)), 200
