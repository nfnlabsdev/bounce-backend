from sqlalchemy.sql import exists
from app import db
from flask import request, jsonify, make_response, session, Response
import os
import json
from datetime import date, datetime

from app.Models.vehicle import Vehicle
from app.Models.user import User
from app.Models.theft_alert import TheftAlert
from app.Models.user_vehicle import UserVehicle
from app.Models.vcu_vehicle import VcuVehicle
from app.Models.vcu_detail import VcuDetail
from app.Models.dms_handshake import DmsHandshake
from app.Models.notification_token import NotificationToken
from app.Models.vehicle_stat import VehicleStat
from app.Models.vehicle_error import VehicleError
from app.Controller import mqtt_controller, user_controller, geofence_controller

def lists():
    res = {}
    try:
        datas = Vehicle.query.all()
        data = vehicle_json_generate(datas)
        res['data'] = data
        # res['message'] = "Data found!"
        res['message'] = session["user_object"]
        return make_response(jsonify(res)), 200
    except Exception as e:
        res['data'] = None
        res['message'] = str(e)
        return make_response(jsonify(res)), 400

def save():
    res = {}
    try:
        user_id = request.form.get('user_id')
        auth_id = request.form.get('auth_id')
        vcu_id = request.form.get('vcu_id')

        # data = [{'user_id': user_id, 'auth_id': auth_id, 'vcu_id': vcu_id}]

        save = Vehicle(user_id=request.form.get('user_id'),
                        auth_id=request.form.get('auth_id'),
                        vcu_id=request.form.get('vcu_id'),
                        register_number=request.form.get('register_number'),
                        color=request.form.get('color'),
                        subscription_model=request.form.get('subscription_model'),
                        purchase_date=request.form.get('purchase_date'),
                        city=request.form.get('city'),
                        immobilise=request.form.get('immobilise'),
                        lock_status=request.form.get('lock_status'),
                        is_active=request.form.get('is_active')
                        )
        db.session.add(save)
        db.session.commit()

        res['data'] = "data"
        res['message'] = "Data added successfully !"
        return make_response(jsonify(res)), 200

    except Exception as e:
        res['data'] = None
        res['message'] = str(e)
        return make_response(jsonify(res)), 400

def dms_data_post():
    res = {}
    try:
        request_data = request.json["data"]
        if session["user_object"]["uid"] != 157822:
            return Response('Authorization failed', status=401)

        #Check user present
        user_exists = db.session.query(db.exists().where(User.phonenumber == request_data["phone_number"])).scalar()
        if user_exists == True:
            res['data'] = None
            res['message'] = "User already available"
            return make_response(jsonify(res)), 400

        #Check vehicle present
        vehicle_exists = db.session.query(db.exists().where(Vehicle.vcu_id == request_data["vcu_id"])).scalar()
        if vehicle_exists == True:
            res['data'] = None
            res['message'] = "vehicle already available"
            return make_response(jsonify(res)), 400

        #Adding the user
        save_user = User(first_name=request_data["first_name"],
                            last_name=request_data["last_name"],
                            phonenumber=request_data["phone_number"])
        db.session.add(save_user)
        db.session.commit()

        #Adding vehicle
        save_vehicle = Vehicle(user_id=save_user.id,
                        auth_id=request_data["legacy_id"],
                        vcu_id=request_data["vcu_id"],
                        register_number=request_data["license_plate"],
                        color=request_data["color"],
                        variant_name=request_data["bike_model"],
                        purchase_date=request_data["purchase_date"],
                        city=request_data["city"],
                        subscription_model="Swapping Model",
                        with_battery=request_data["with_battery"]
                        )
        db.session.add(save_vehicle)
        db.session.commit()

        # Adding user_vehicle mapping
        user_vehicle_obj = UserVehicle(user_id=save_user.id,
                                       vehicle_id=save_vehicle.id,
                                       start_date=request_data["purchase_date"]
                                    )
        db.session.add(user_vehicle_obj)
        db.session.commit()

        # Adding vcu_stat
        user_vehicle_stat = VehicleStat(vcu_id=request_data["vcu_id"])
        db.session.add(user_vehicle_stat)
        db.session.commit()

        # Adding vcu_details
        vcu_details_obj = VcuDetail(vcu_id=request_data["vcu_id"],
                                      imei_code=request_data["imei_code"],
                                      vcu_type=request_data["vcu_type"]
                                    )
        db.session.add(vcu_details_obj)
        db.session.commit()

        # Adding vcu_vehicles mapping
        vcu_vehicles_obj = VcuVehicle(vcu_details_id=vcu_details_obj.id,
                                       vehicle_id=save_vehicle.id,
                                       start_date=request_data["purchase_date"]
                                       )
        db.session.add(vcu_vehicles_obj)
        db.session.commit()

        #Adding dms_handshakes entry
        dms_handshakes_obj = DmsHandshake(vcu_id=request_data["vcu_id"],
                                          purpose="vehicle register",
                                          dms_content=json.dumps(request.json)
                                            )
        db.session.add(dms_handshakes_obj)
        db.session.commit()

        #subscribe the topic
        subscribe_vcu = mqtt_controller.new_user_subscribe(request_data["vcu_id"])

        res['user_id'] = save_user.id
        res['vehicle_id'] = save_vehicle.id
        res['vcu_id'] = request_data["vcu_id"]
        res['message'] = "Success"
        return make_response(jsonify(res)), 200
    except Exception as e:
        res['data'] = None
        res['message'] = str(e)
        return make_response(jsonify(res)), 400

def dms_data_update():
    res = {}
    # try:
    print(session["vehicle_object"])
    request_data = request.json["data"]
    if session["user_object"]["uid"] != 157822:
        return Response('Authorization failed', status=401)

    vehicle_obj = Vehicle.query.filter_by(auth_id=request_data["legacy_id"], is_active=True).first()
    if vehicle_obj is None:
        res['data'] = None
        res['message'] = "vehicle not available"
        return make_response(jsonify(res)), 400

    user_exist_obj = User.query.filter_by(phonenumber=request_data["phone_number"]).first()
    if user_exist_obj is not None:
        if user_exist_obj.id != vehicle_obj.user_id:
            res['data'] = None
            res['message'] = "Phone already available"
            return make_response(jsonify(res)), 400

    old_vcu = vehicle_obj.vcu_id

    if (vehicle_obj.vcu_id == request_data["vcu_id"]):
        #Update vehicle data
        # vehicle_obj.vcu_id=request_data["vcu_id"],
        vehicle_obj.register_number=request_data["license_plate"],
        vehicle_obj.color=request_data["color"],
        vehicle_obj.variant_name=request_data["bike_model"],
        vehicle_obj.purchase_date=request_data["purchase_date"],
        vehicle_obj.city=request_data["city"],
        vehicle_obj.with_battery=request_data["with_battery"]
        db.session.commit()
    else:

        vehicle_vcu_obj = Vehicle.query.filter_by(vcu_id=request_data["vcu_id"]).first()
        if vehicle_vcu_obj is not None:
            res['data'] = None
            res['message'] = "VCU already available"
            return make_response(jsonify(res)), 400

        #disable_Vehicle
        vehicle_obj.is_active=False
        db.session.commit()

        #Adding vehicle
        save_vehicle = Vehicle(user_id=vehicle_obj.user_id,
                        auth_id=request_data["legacy_id"],
                        vcu_id=request_data["vcu_id"],
                        register_number=request_data["license_plate"],
                        color=request_data["color"],
                        variant_name=request_data["bike_model"],
                        purchase_date=request_data["purchase_date"],
                        city=request_data["city"],
                        subscription_model="Swapping Model",
                        with_battery=request_data["with_battery"],
                        geofence=vehicle_obj.geofence,
                        anti_theft=vehicle_obj.anti_theft,
                        country_code=vehicle_obj.country_code,
                        owner_model=vehicle_obj.owner_model,
                        payment_type=vehicle_obj.payment_type,
                        immobilise=vehicle_obj.immobilise
                        )
        db.session.add(save_vehicle)
        db.session.commit()

        #Remove vcu_vehicles mapping
        vcu_vehicle_obj = VcuVehicle.query.filter_by(vehicle_id=vehicle_obj.id, is_active=True).first()
        vcu_vehicle_obj.end_date = datetime.now(),
        vcu_vehicle_obj.is_active = False
        db.session.commit()

        # Adding vcu_stat
        user_vehicle_stat = VehicleStat(vcu_id=save_vehicle.vcu_id)
        db.session.add(user_vehicle_stat)
        db.session.commit()

        # Adding vcu_details
        vcu_details_obj = VcuDetail(vcu_id=save_vehicle.vcu_id,
                                      imei_code=request_data["imei_code"],
                                      vcu_type=request_data["vcu_type"]
                                    )
        db.session.add(vcu_details_obj)
        db.session.commit()

        # Adding vcu_vehicles mapping
        vcu_vehicles_obj = VcuVehicle(vcu_details_id=vcu_details_obj.id,
                                       vehicle_id=save_vehicle.id,
                                       start_date=request_data["purchase_date"]
                                       )
        db.session.add(vcu_vehicles_obj)
        db.session.commit()

    #Update user data
    user_obj = User.query.filter_by(id=vehicle_obj.user_id).first()
    user_obj.first_name=request_data["first_name"],
    user_obj.last_name=request_data["last_name"],
    user_obj.phonenumber=request_data["phone_number"]
    db.session.commit()

    #Adding dms_handshakes entry
    dms_handshakes_obj = DmsHandshake(vcu_id=request_data["vcu_id"],
                                      purpose="vehicle update",
                                      dms_content=json.dumps(request.json))

    db.session.add(dms_handshakes_obj)
    db.session.commit()

    #subscribe/unsubscribe the topic
    subscribe_vcu = mqtt_controller.new_user_subscribe(request_data["vcu_id"])
    unsubscribe_vcu = mqtt_controller.unsubscibe_to_vcu(old_vcu)

    res['message'] = "Vehicle & User updated"
    return make_response(jsonify(res)), 200

    # except Exception as e:
    #     res['data'] = None
    #     res['message'] = str(e)
    #     return make_response(jsonify(res)), 400

def dms_vcu_update():
    res = {}
    # try:
    print(session["vehicle_object"])
    request_data = request.json["data"]
    if session["user_object"]["uid"] != 157822:
        return Response('Authorization failed', status=401)

    vehicle_obj = Vehicle.query.filter_by(auth_id=request_data["legacy_id"], is_active=True).first()
    if vehicle_obj is None:
        res['data'] = None
        res['message'] = "vehicle not available"
        return make_response(jsonify(res)), 400

    vehicle_vcu_obj = Vehicle.query.filter_by(vcu_id=request_data["vcu_id"]).first()
    if vehicle_vcu_obj is not None:
        res['data'] = None
        res['message'] = "VCU already available"
        return make_response(jsonify(res)), 400

    #disable_Vehicle
    vehicle_obj.is_active=False
    db.session.commit()

    old_vcu = vehicle_obj.vcu_id

    #Adding vehicle
    save_vehicle = Vehicle(user_id=vehicle_obj.user_id,
                    auth_id=request_data["legacy_id"],
                    vcu_id=request_data["vcu_id"],
                    register_number=vehicle_obj.register_number,
                    color=vehicle_obj.color,
                    variant_name=vehicle_obj.variant_name,
                    purchase_date=vehicle_obj.purchase_date,
                    city=vehicle_obj.city,
                    subscription_model="Swapping Model",
                    with_battery=vehicle_obj.with_battery,
                    geofence=vehicle_obj.geofence,
                    anti_theft=vehicle_obj.anti_theft,
                    country_code=vehicle_obj.country_code,
                    owner_model=vehicle_obj.owner_model,
                    payment_type=vehicle_obj.payment_type,
                    immobilise=vehicle_obj.immobilise
                    )
    db.session.add(save_vehicle)
    db.session.commit()

    #Remove vcu_vehicles mapping
    vcu_vehicle_obj = VcuVehicle.query.filter_by(vehicle_id=vehicle_obj.id, is_active=True).first()
    vcu_vehicle_obj.end_date = datetime.now(),
    vcu_vehicle_obj.is_active = False
    db.session.commit()

    # Adding vcu_stat
    user_vehicle_stat = VehicleStat(vcu_id=save_vehicle.vcu_id)
    db.session.add(user_vehicle_stat)
    db.session.commit()

    # Adding vcu_details
    vcu_details_obj = VcuDetail(vcu_id=save_vehicle.vcu_id,
                                  imei_code=request_data["imei_code"],
                                  vcu_type=request_data["vcu_type"]
                                )
    db.session.add(vcu_details_obj)
    db.session.commit()

    # Adding vcu_vehicles mapping
    vcu_vehicles_obj = VcuVehicle(vcu_details_id=vcu_details_obj.id,
                                   vehicle_id=save_vehicle.id,
                                   start_date=date.today()
                                   )
    db.session.add(vcu_vehicles_obj)
    db.session.commit()

    #Adding dms_handshakes entry
    dms_handshakes_obj = DmsHandshake(vcu_id=request_data["vcu_id"],
                                      purpose="VCU update",
                                      dms_content=json.dumps(request.json))

    db.session.add(dms_handshakes_obj)
    db.session.commit()

    #subscribe/unsubscribe the topic
    subscribe_vcu = mqtt_controller.new_user_subscribe(request_data["vcu_id"])
    unsubscribe_vcu = mqtt_controller.unsubscibe_to_vcu(old_vcu)

    res['message'] = "New VCU Updated"
    return make_response(jsonify(res)), 200

    # except Exception as e:
    #     res['data'] = None
    #     res['message'] = str(e)
    #     return make_response(jsonify(res)), 400

def dms_data_delete():
    res = {}
    try:
        if session["user_object"]["uid"] != 157822:
            return Response('Authorization failed', status=401)
        #Adding dms_handshakes entry
        dms_handshakes_obj = DmsHandshake(purpose="vehicle delete",
                                          dms_content=json.dumps(request.json))

        db.session.add(dms_handshakes_obj)
        db.session.commit()

        vehicle_obj = Vehicle.query.filter_by(auth_id=request.args.get('legacy_id')).first()
        if vehicle_obj is None:
            res['data'] = None
            res['message'] = "vehicle not available"
            return make_response(jsonify(res)), 400

        #Update vehicle data
        vehicle_obj.is_active=False
        db.session.commit()

        res['message'] = "Vehicle & User Deleted"
        return make_response(jsonify(res)), 200

    except Exception as e:
        res['data'] = None
        res['message'] = str(e)
        return make_response(jsonify(res)), 400

def vehicle_manual():
    res = {}
    veh_list = []
    base_url = request.base_url.split('vehicle_manual')[0] + 'file/'
    try:
        res_one = {}
        res_one['image'] = base_url + 'Infinity-Black.png'
        res_one['description'] = "The suggested tyre pressure should be 22psi in the front tyre and 29psi"
        res_one['title'] = "Air Tyre Pressure (Front)"
        res_one['side_value'] = "26 PSI"
        res_one['link'] = 'https://bd.gaadicdn.com/upload/E-brochure/Bounce%20Infinity%20E1/835-brochure.pdf'
        veh_list.append(res_one)

        res_two = {}
        res_two['image'] = base_url + 'Infinity-Black.png'
        res_two['description'] = "The suggested tyre pressure should be 22psi in the front tyre and 29psi"
        res_two['title'] = "Air Tyre Pressure (Back)"
        res_two['side_value'] = "28 PSI"
        res_two['link'] = 'https://bd.gaadicdn.com/upload/E-brochure/Bounce%20Infinity%20E1/835-brochure.pdf'
        veh_list.append(res_two)

        res_three = {}
        res_three['image'] = base_url + 'Infinity-Black.png'
        res_three['description'] = "Distinguishing personality that shines through your ride"
        res_three['title'] = "Iconic Headlamp"
        res_three['side_value'] = ""
        res_three['link'] = 'https://bd.gaadicdn.com/upload/E-brochure/Bounce%20Infinity%20E1/835-brochure.pdf'
        veh_list.append(res_three)

        res['data'] = veh_list
        res['status'] = True
        return make_response(jsonify(res)), 200
    except Exception as e:
        res['data'] = None
        res['message'] = str(e)
        return make_response(jsonify(res)), 400

def vcu_errors():
    res = {}
    try:
        vcu_id = session["vehicle_object"]["vcu_id"]
        # vehicle_err_obj = VehicleError.query.filter_by(vcu_id=vcu_id).order_by(VehicleError.id.desc()).limit(20)
        vehicle_err_obj = VehicleError.query.filter_by(is_active=True,vcu_id=vcu_id).distinct(VehicleError.title).limit(20)
        res['data'] = error_json_generate(vehicle_err_obj)
        res['status'] = True
        res['message'] = "Error list"
        return make_response(jsonify(res)), 200
    except Exception as e:
        res['data'] = None
        res['message'] = str(e)
        return make_response(jsonify(res)), 400


def notification_token():
    res = {}
    try:

        vcu_id = session["vehicle_object"]["vcu_id"]
        request_data = request.json["data"]
        notification_obj = NotificationToken.query.filter_by(vcu_id=vcu_id).first()
        if notification_obj is not None:

            notification_obj.token_str=request_data["gcm_token"]
            db.session.commit()

            res['status'] = True
            res['message'] = "Notification token updated"
            return make_response(jsonify(res)), 200
        else:
            save_token = NotificationToken(vcu_id=vcu_id, token_str=request_data["gcm_token"])
            db.session.add(save_token)
            db.session.commit()
            res['status'] = True
            res['message'] = "Notification token added"
            return make_response(jsonify(res)), 200

    except Exception as e:
        res['data'] = None
        res['message'] = str(e)
        return make_response(jsonify(res)), 400

def file_url(path):
    return send_from_directory("/files/",path)

def vehicle_json_generate(values):
    return [
        {"id": i.id, "user_id": i.user_id, "auth_id": i.auth_id, "vcu_id": i.vcu_id, "register_number": i.register_number, "color": i.color, "subscription_model": i.subscription_model,"purchase_date": i.purchase_date,"city": i.city, "immobilise": i.immobilise, "geofence": i.geofence, "vehicle_mode": i.vehicle_mode, "anti_theft": i.anti_theft, "anti_theft_disable_till": i.anti_theft_disable_till, "lock_status": i.lock_status,"is_active": i.is_active}
        for i in values
    ]

def error_json_generate(values):
    return [
        {"id": i.id, "title": i.title, "code": i.code, "image": "https://thumbs.dreamstime.com/z/bike-bicycle-air-pump-flat-vector-icon-symbol-pictogram-sign-blue-monochrome-design-editable-stroke-accessory-light-style-132145137.jpg", "error_type": i.error_type, "details": "More description paragraph text goes here. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum", "description": "The battery encountered some technical problem and error text goes here.", "created_at": i.created_at}
        for i in values
    ]
